<?php
  include "./partials/head.php";
  include "./partials/nav.php";
?>
    <section id="header-cotizador" class="parallax-container">
        <div class="section no-pad-bot">
            <div class="container">
                <div class="row">
                    <div class="col s12">
                        <h2 class="header white-text">
                            <span class="line">
                                <img src="./assets/img/line.png" alt="">P</span>ersonas
                        </h2>
                        <h5 class="white-text">SEGUROS Y PLANES A DISEÑADOS A TU MEDIDA</h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="parallax blue">
            <img src="./assets/img/packbaionline.jpg" class="opacity-2" alt="Unsplashed background img 1">
        </div>
    </section>
    <section id="services-personas">
        <?php
            include "./layouts/personas.php";
        ?>
    </section>

    <?php
  include "./layouts/contact.php";
  include "./partials/footer.php";
?>