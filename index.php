<?php
  include "./partials/head.php";
  include "./partials/nav.php";
?>

    <div id="index-banner" class="parallax-container">
        <div class="section no-pad-bot">
            <div class="container">
                <h1 class="header center white-text">Elegí tu
                    <strong>Seguro ideal</strong>
                </h1>
                <div class="row">
                    <div class="col s12 m3 no-pad">
                        <label>
                            <span class="label-barra">Provincia</span>
                            <div id="provincia-select">
                                <select id="provincia" class="provincia-select" class="validate" required>
                                    <option value="2018">Provincia 1</option>
                                    <option value="2018">Provincia 2</option>
                                </select>
                            </div>
                        </label>
                    </div>
                    <div class="col s12 m3 no-pad">
                        <label>
                            <span class="label-barra">Partido</span>
                            <div id="partido-select">
                                <select id="partido" class="partido-select" class="validate" required>
                                    <option value="2018">Partido 1</option>
                                    <option value="2018">Partido 2</option>
                                </select>
                            </div>
                        </label>
                    </div>
                    <div class="col s12 m3 no-pad">
                        <label>
                            <span class="label-barra">Localidad</span>
                            <div id="localidad-select">
                                <select id="localidad" class="localidad-select" class="validate" required>
                                    <option value="2018">Localidad 1</option>
                                    <option value="2018">Localidad 2</option>
                                </select>
                            </div>
                        </label>
                    </div>
                    <div class="col s12 m3 no-pad">
                        <button class="btn waves-effect waves-light orange select2 buscar-barra">
                            Buscar
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="parallax">
            <img src="./assets/img/background2.jpg" alt="Unsplashed background img 1">
        </div>
    </div>


    <!-- <section id="services">


    </section> -->

    <!-- <section id="pack-baionline" class="parallax-container load">
        <div class="section no-pad-bot">
            <div class="container">
                <div class="row">
                    <div class="col s12 center-align">
                        <h2 class="header center white-text">
                            <span class="line">
                                <img src="./assets/img/line.png" alt="">P</span>ackBaionline
                        </h2>
                        <br>
                        <p class="white-text center-align">
                            Juntá todos tu seguros y crea el PackBaionline - Consultá con tu Asesor Baionline, y obtené un importante descuento en todos
                            tus Seguros!
                        </p>
                        <br>
                        <a class="waves-effect btn-large white blue-text btn modal-trigger" href="#modal2">Me interesa</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="parallax blue">
            <img src="./assets/img/packbaionline.jpg" class="opacity-2" alt="Unsplashed background img 1">
        </div>
    </section> -->


    <section id="info">
        <div class="container">
            <div class="row no-margin-row">

                <div class="col s12 m6 quienes-somos no-pad grey darken-4">
                    <div class="container">
                        <h2 class="header left-align white-text">
                            <span class="line">
                                <img src="./assets/img/line.png" alt="">Q</span>uiénes somos
                        </h2>
                        <p class="white-text">
                            Un único lugar para todos los seguros que necesitas Somos una plataforma online donde podes contratar seguros para tu
                            <span
                                class="orange-text">persona, auto y empresa,</span> de manera profesional, sencilla y segura. Encontrando el productor
                                asesor de seguros que
                                <span class="orange-text">satisfaga tus requerimientos.</span>
                        </p>
                    </div>
                </div>

                <div class="col s12 m6 no-pad quienes-somos-img">
                    <div class="background">
                        <img src="./assets/img/quienes-somos.jpg" alt="">
                    </div>
                </div>

            </div>

            <div class="row no-margin-row">

                <div class="col s12 m6 no-pad simple-img">
                    <div class="background">
                        <img src="./assets/img/simple.jpg" alt="">
                    </div>
                </div>

                <div class="col s12 m6 simple no-pad white">
                    <div class="container">
                        <h2 class="header left-align no-shadow">
                            <span class="line">
                                <img src="./assets/img/line.png" alt="">S</span>imple
                        </h2>
                        <ul>
                            <li>
                                <span class="material-icons orange-text">touch_app</span>Selecciona el seguro que necesitas</li>
                            <li>
                                <span class="material-icons blue-text">search</span>Encontra al experto en tu seguro y soluciona tu problema.</li>
                            <li>
                                <span class="material-icons green-text">spellcheck</span>Una vez finalizada la atencion, calificalo de acuerdo a su atención y ayudanos
                                a generar una reputación de confianza para futuros usuarios.</li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section id="confiables">
        <div class="had-container">
            <div class="row no-margin-row">
                <div class="col s12 m6 center-align white confiables-box">
                    <div class="container">
                        <h2 class="header blue-text no-shadow">
                            <span class="line">
                                <img src="./assets/img/line.png" alt="">C</span>onfiables
                        </h2>
                        <p>
                            Todos los productores cuentan con la norma
                            <a href="#" class="orange-text">
                                <strong>Baionline.</strong>
                            </a>
                        </p>
                    </div>
                </div>
                <div class="col s12 m6 center-align grey darken-4 solventes-box">
                    <div class="container">
                        <h2 class="header white-text no-shadow">
                            <span class="line">
                                <img src="./assets/img/line.png" alt="">S</span>olventes
                        </h2>
                        <p class="white-text">
                            Contamos con mas de
                            <strong class="orange-text">456</strong> productores asesores en seguros matriculados y mas de
                            <strong class="orange-text">1234</strong> compañias que respaldan el modelo de negocio.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="work">
        <div class="container">
            <div class="row">
                <div class="col m12">
                    <div class="card">
                        <div class="container">
                            <div class="card-content">
                                <h2 class="header no-shadow">
                                    <span class="line">
                                        <img src="./assets/img/line.png" alt="">W</span>ork with us
                                </h2>
                                <p>
                                    <strong>
                                        ¿ Sos productor de seguros matriculado?, esta plataforma esta hecha para vos! <span class="orange-text"><a href="#">Mira nuestros planes</a></span>  y empeza a generar
                                        clientes.
                                    </strong>
                                </p>
                                <br>
                                <p>

                                    <h5>
                                        ¿Cómo funciona?
                                    </h5>

                                    <div class="row">
                                        <div class="col s6 m3 center-align red-text">
                                            <i class="large material-icons">filter_1</i>
                                            <br> Ingresa a nuestro panel de clientes
                                        </div>
                                        <div class="col s6 m3 center-align blue-text text-accent-3">
                                            <i class="large material-icons">filter_2</i>
                                            <br> Selecciona un trabajo
                                        </div>
                                        <div class="col s6 m3 center-align teal-text">
                                            <i class="large material-icons">filter_3</i>
                                            <br> Contáctate con el cliente
                                        </div>
                                        <div class="col s6 m3 center-align orange-text">
                                            <i class="large material-icons">filter_4</i>
                                            <br> Calificalo y seras calificado
                                        </div>
                                    </div>
                                    Si surge algún incoveniente con una contratacion. No te preocupes, la mayoría de las veces se trata de simples malentendidos
                                    fáciles de resolver. Te informaremos al respecto y te diremos cómo seguir. De necesitarlo,
                                    podremos mediar hasta llegar a una solución.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <div id="contact">
        <div id="modal2" class="modal modal-fixed-footer" style="width:500px; z-index: 1005; display: none; opacity: 0; transform: scaleX(0.7); top: 4%;">
            <div class="modal-content">
                <div class="row">
                    <h6 class="blue-text">
                        Contactate con nosotros interesado en
                        <b>PackBaionline</b>
                    </h6>
                    <div class="input-field col s12 m6 grey-text">
                        <i class="material-icons prefix">account_circle</i>
                        <input id="icon_prefix" type="text" class="validate">
                        <label for="icon_prefix">Nombre</label>
                    </div>
                    <div class="input-field col s12 m6 grey-text">
                        <i class="material-icons prefix">account_circle</i>
                        <input id="icon_prefix" type="text" class="validate">
                        <label for="icon_prefix">Apellido</label>
                    </div>

                    <div class="input-field col s12 grey-text">
                        <i class="material-icons prefix">email</i>
                        <input id="email" type="email" data-error="Email inválido" data-success="right" class="validate">
                        <label for="email">Email</label>
                    </div>


                </div>
                <div class="row">
                    <div class="input-field col s12 m6 grey-text">
                        <i class="material-icons prefix">phone</i>
                        <input id="icon_telephone" type="tel" class="validate">
                        <label for="icon_telephone">Teléfono</label>
                    </div>
                    <!-- Switch -->
                    <div class="switch col s12 m6">
                        <label>
                            <div class="chip chip-large green white-text">
                                <span class="fa fa-whatsapp"></span> Whatsapp
                            </div>
                            <br> No
                            <input type="checkbox">
                            <span class="lever"></span>
                            Si
                        </label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="#!" class="btn-flat modal-action modal-close waves-effect left">Cerrar</a>
                <button href="#!" class="btn modal-action modal-close waves-effect orange" type="submit">Enviar</button>
            </div>
        </div>
    </div>




    <?php
  include "./layouts/contact.php";
  include "./partials/footer.php";
?>