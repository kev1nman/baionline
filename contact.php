<?php
  include "./partials/head.php";
  include "./partials/nav.php";
?>

    <section id="work">
        <div class="container">
            <div class="row">
                <div class="col m12">
                    <div class="card">
                        <div class="container">
                            <div class="card-content">
                                <h2 class="header no-shadow">
                                    <span class="line">
                                        <img src="./assets/img/line.png" alt="">C</span>ontactate con nosotros
                                </h2>
                                <h5>Interesado en <b>PackBaionline</b></h5>
                                <div id="contact">
                                    <div class="row">
                                        <div class="input-field col s12 m6 grey-text">
                                            <i class="material-icons prefix">account_circle</i>
                                            <input id="icon_prefix" type="text" class="validate">
                                            <label for="icon_prefix">Nombre</label>
                                        </div>
                                        <div class="input-field col s12 m6 grey-text">
                                            <i class="material-icons prefix">account_circle</i>
                                            <input id="icon_prefix" type="text" class="validate">
                                            <label for="icon_prefix">Apellido</label>
                                        </div>

                                        <div class="input-field col s12 grey-text">
                                            <i class="material-icons prefix">email</i>
                                            <input id="email" type="email" data-error="Email inválido" data-success="right" class="validate">
                                            <label for="email">Email</label>
                                        </div>


                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12 m6 grey-text">
                                            <i class="material-icons prefix">phone</i>
                                            <input id="icon_telephone" type="tel" class="validate">
                                            <label for="icon_telephone">Teléfono</label>
                                        </div>
                                        <!-- Switch -->
                                        <div class="switch col s12 m6">
                                            <label>
                                                <div class="chip chip-large green white-text">
                                                    <span class="fa fa-whatsapp"></span> Whatsapp
                                                </div>
                                                <br> No
                                                <input type="checkbox">
                                                <span class="lever"></span>
                                                Si
                                            </label>
                                        </div>
                                    </div>

                                    <button href="#!" class="btn modal-action modal-close waves-effect orange right" type="submit">Enviar</button>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    
    <?php
    include "./layouts/contact.php";
    include "./partials/footer.php";
    ?>