const gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    browserSync = require('browser-sync').create(),
    minifyCSS = require('gulp-minify-css');

gulp.task('sass', () => {
    gulp.src('./sass/*.scss')
        .pipe(sass({
            outputStyle: 'compressed'
        }))
        .pipe(autoprefixer({
            versions: ['lats 2 browsers']
        }))
        .pipe(gulp.dest('./assets/css'));
});

gulp.task('default', () => {
    browserSync.init({
        proxy: 'http://localhost/baionline'
    });
    gulp.watch('./*.php').on('change', browserSync.reload);
    gulp.watch('./partials/*.php').on('change', browserSync.reload);
    gulp.watch('./layouts/*.php').on('change', browserSync.reload);
    gulp.watch('./assets/css/*.css').on('change', browserSync.reload);
    gulp.watch('./assets/js/*.js').on('change', browserSync.reload);
    gulp.watch('./assets/img/**').on('change', browserSync.reload);
    gulp.watch('./sass/*.scss', ['sass']);
});