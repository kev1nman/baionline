<div class="top-bar hide-on-med-and-down">
    <div class="container">
        <div class="row no-margin-row">
            <div class="col m4 blue">
                <div class="valign-wrapper white-text data">
                    <i class="material-icons">local_phone</i> +54 9 112 345 6789
                </div>
            </div>
            <div class="col m8 blue darken-1">
                <div class="valign-wrapper white-text data">
                    <i class="material-icons">mail</i> contact@baionline.com.ar
                </div>
            </div>
        </div>
    </div>
</div>
<div id="menu">
    <nav class="menu white" role="navigation" data-target="menu">

        <div class="nav-wrapper container">
            <a id="logo-container" href="index.php" class="brand-logo">
                <img class="logo" src="./assets/img/logo-blanco.png" alt="">
            </a>
            <ul class="right hide-on-med-and-down">
                <li>
                    <a href="personas.php">
                        <i class="material-icons left">people</i>Personas</a>
                </li>
                <li>
                    <a href="empresas.php">
                        <i class="material-icons left">work</i>Empresa</a>
                </li>
                <li>
                    <a href="auto.php">
                        <i class="material-icons left">directions_car</i>Auto</a>
                </li>
                <!-- <li>
                    <a class="waves-effect waves-light orange btn">Login</a>
                </li> -->
            </ul>

            <ul id="nav-mobile" class="side-nav">
                <div class="top-bar top-bar-mobile">
                    <div class="container">
                        <div class="row no-margin-row">
                            <div class="col s12 m4 blue">
                                <div class="valign-wrapper white-text data">
                                    <i class="material-icons">local_phone</i> +54 9 112 345 6789
                                </div>
                            </div>
                            <div class="col s12 m8 blue darken-1">
                                <div class="valign-wrapper white-text data">
                                    <i class="material-icons">mail</i> contact@baionline.com.ar
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <li>
                    <a href="personas.php">
                        <i class="material-icons left">people</i>Personas</a>
                </li>
                <li>
                    <a href="empresas.php">
                        <i class="material-icons left">work</i>Empresa</a>
                </li>
                <li>
                    <a href="auto.php">
                        <i class="material-icons left">directions_car</i>Auto</a>
                </li>
                <!-- <li>
                    <a class="waves-effect waves-light orange btn">Login</a>
                </li> -->
            </ul>
            <a href="#" data-activates="nav-mobile" class="button-collapse">
                <i class="material-icons">menu</i>
            </a>
        </div>
    </nav>
</div>