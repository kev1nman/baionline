<footer class="page-footer white">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <h5 class="white-text">Footer</h5>
                <img class="logo" src="./assets/img/logo-blanco.png" alt="">
                <br>
                <br>
                <!-- <div class="row valign-wrapper grey-text text-darken-1">
                    <div class="col s2">
                        <i class="medium material-icons">access_time</i>
                    </div>
                    <div class="col s10">
                        <span>
                            Mon-Sat: 9:30-14:30, 16:30-20:30 Sun: 10:00-14:00
                        </span>
                    </div>
                </div>
                <div class="row valign-wrapper grey-text text-darken-1">
                    <div class="col s2">
                        <i class="medium material-icons">location_on</i>
                    </div>
                    <div class="col s10">
                        <span>
                            Intersection of Charling Cross Road, Strand, WC2, Moscow
                        </span>
                    </div>
                </div> -->

            </div>
            <div class="col l3 s12 grey-text text-darken-1">
                <br>
                <br>
                <p>
                    Un único lugar para todos los seguros que necesitas Somos una plataforma online donde podes contratar seguros para tu persona,
                    auto y empresa, de manera profesional, sencilla y segura.</p>
            </div>
            <div class="col l3 s12">
                <h5 class="white-text">-</h5>
                <div class="col s3">
                    <a class="waves-effect waves-light btn-floating social twitter">
                        <i class="fa fa-twitter"></i> Sign in with twitter</a>
                </div>
                <div class="col s3">
                    <a class="waves-effect waves-light btn-floating social facebook">
                        <i class="fa fa-facebook"></i> Sign in with facebook</a>
                </div>
                <div class="col s3">
                    <a class="waves-effect waves-light btn-floating social google">
                        <i class="fa fa-google"></i> Sign in with google</a>
                </div>
                <div class="col s3">
                    <a class="waves-effect waves-light btn-floating social instagram">
                        <i class="fa fa-instagram"></i> Sign in with instagram</a>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright grey darken-4">
        <div class="container">
            © Este sitio fue desarrollado por
            <a class="blue-text text-lighten-1" href="#">Wapicom</a>. Baionline 2018.

        </div>
    </div>
</footer>


<!--  Scripts-->
<script src="./assets/js/jquery/jquery-2.2.4.min.js"></script>
<script src="./assets/js/materialize.min.js"></script>
<script src="./assets/plugins/materialize-stepper/jquery.validate.min.js"></script>
<script src="./assets/plugins/materialize-stepper/materialize-stepper.min.js"></script>
<script src="./assets/js/utils.js"></script>

<script src="./assets/plugins/materialize-stepper/jquery.validate.min.js" type="text/javascript"></script>
<script src="./assets/plugins/select2/js/select2.full.min.js"></script>
<script src="./assets/js/utils.js"></script>
<script src="./assets/js/quoteengine.js"></script>


<script src="./assets/js/custom.js"></script>

<script>
    $(function () {
        $('.stepper').activateStepper();
    });
</script>

</body>

</html>