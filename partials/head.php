<!DOCTYPE html>
<html lang="es">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />

  <title>Baionline</title>


  <link rel="apple-touch-icon" sizes="57x57" href="./assets/icon/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="./assets/icon/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="./assets/icon/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="./assets/icon/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="./assets/icon/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="./assets/icon/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="./assets/icon/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="./assets/icon/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="./assets/icon/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="./assets/icon/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="./assets/icon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="./assets/icon/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="./assets/icon/favicon-16x16.png">
  <link rel="manifest" href="./assets/icon/manifest.json">
  <meta name="msapplication-TileColor" content="#09098B">
  <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
  <meta name="theme-color" content="#09098B">
  
  <link rel="stylesheet" href="./assets/plugins/select2/css/select2.min.css" type="text/css">
  <link rel="stylesheet" href="./assets/plugins/select2-materialize/select2-materialize.css" type="text/css">


  <!-- CSS  -->
  <link href="./assets/css/material-icons.css" rel="stylesheet">
  <link rel="stylesheet" href="./assets/css/font-awesome.css">
  <link href="./assets/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection" />
  <link rel="stylesheet" href="./assets/plugins/materialize-social/materialize-social.css">
  <link rel="stylesheet" href="./assets/plugins/materialize-stepper/materialize-stepper.min.css">
  

</head>

<body>