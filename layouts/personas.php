<div class="container">
    <div class="section">

        <!--   Icon Section   -->
        <div class="row">
            <div class="col s12 m4">
                <a href="contact.php">
                    <div class="card hoverable">
                        <div class="card-image center-align">
                            <img src="./assets/img/vida.jpg">
                            <a href="contact.php" class="btn-floating btn-large halfway-fab waves-effect white">
                                <i class="material-icons blue-text">people</i>
                            </a>
                        </div>
                        <div class="card-content center-align">
                            <a href="contact.php" class="blue-text">Vida</a>
                            <i> Desde $ 000.00 por mes</i>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col s12 m4">
                <a href="contact.php">
                    <div class="card hoverable">
                        <div class="card-image center-align">
                            <img src="./assets/img/hogar.jpg">
                            <a href="contact.php" class="btn-floating btn-large halfway-fab waves-effect white">
                                <i class="material-icons blue-text">people</i>
                            </a>
                        </div>
                        <div class="card-content center-align">
                            <a href="contact.php" class="blue-text">Hogar</a>
                             <i> Desde $ 000.00 por mes</i>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col s12 m4">
                <a href="contact.php">
                    <div class="card hoverable">
                        <div class="card-image center-align">
                            <img src="./assets/img/auto.jpg">
                            <a href="contact.php" class="btn-floating btn-large halfway-fab waves-effect white">
                                <i class="material-icons blue-text">people</i>
                            </a>
                        </div>
                        <div class="card-content center-align">
                            <a href="contact.php" class="blue-text">Auto</a>
                             <i> Desde $ 000.00 por mes</i>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col s12 m4">
                <a href="contact.php">
                    <div class="card hoverable">
                        <div class="card-image center-align">
                            <img src="./assets/img/moto.jpg">
                            <a href="contact.php" class="btn-floating btn-large halfway-fab waves-effect white">
                                <i class="material-icons blue-text">people</i>
                            </a>
                        </div>
                        <div class="card-content center-align">
                            <a href="contact.php" class="blue-text">moto</a>
                            <i> Desde $ 000.00 por mes</i>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col s12 m4">
                <a href="contact.php">
                    <div class="card hoverable">
                        <div class="card-image center-align">
                            <img src="./assets/img/nautica.jpg">
                            <a href="contact.php" class="btn-floating btn-large halfway-fab waves-effect white">
                                <i class="material-icons blue-text">people</i>
                            </a>
                        </div>
                        <div class="card-content center-align">
                            <a href="contact.php" class="blue-text">Nautica</a>
                            <i> Desde $ 000.00 por mes</i>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col s12 m4">
                <a href="contact.php">
                    <div class="card hoverable">
                        <div class="card-image center-align">
                            <img src="./assets/img/accidentes-personales.jpg">
                            <a href="contact.php" class="btn-floating btn-large halfway-fab waves-effect white">
                                <i class="material-icons blue-text">people</i>
                            </a>
                        </div>
                        <div class="card-content center-align">
                            <a href="contact.php" class="blue-text">Accidentes personales</a>
                             <i> Desde $ 000.00 por mes</i>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col s12 m4">
                <a href="contact.php">
                    <div class="card hoverable">
                        <div class="card-image center-align">
                            <img src="./assets/img/viajes.jpg">
                            <a href="contact.php" class="btn-floating btn-large halfway-fab waves-effect white">
                                <i class="material-icons blue-text">people</i>
                            </a>
                        </div>
                        <div class="card-content center-align">
                            <a href="contact.php" class="blue-text">Viajes</a>
                            <i> Desde $ 000.00 por mes</i>
                        </div>
                    </div>
                </a>
            </div>

        </div>


    </div>
</div>