<section id="contact" class="parallax-container">
    <div class="section no-pad-bot">
        <div class="container">
            <div class="row">
                <div class="col s12 m6">
                    <h2 class="header center white-text">
                        <span class="line">
                            <img src="./assets/img/line.png" alt="">C</span>ontactate con nosotros
                    </h2>
                </div>
                <div class="col s12 m6">
                    <form action="">
                        <div class="input-field col s12 white-text">
                            <i class="material-icons prefix">account_circle</i>
                            <input id="icon_prefix" type="text" class="validate">
                            <label for="icon_prefix">Nombre</label>
                        </div>
                        <div class="input-field col s12 white-text">
                            <i class="material-icons prefix">phone</i>
                            <input id="icon_telephone" type="tel" class="validate">
                            <label for="icon_telephone">Teléfono</label>
                        </div>
                        <div class="input-field col s12 white-text">
                            <i class="material-icons prefix">email</i>
                            <input id="email" type="email" data-error="Email inválido" data-success="right" class="validate">
                            <label for="email">Email</label>
                        </div>
                        <div class="input-field col s12 white-text">
                            <i class="material-icons prefix">mode_edit</i>
                            <textarea id="icon_prefix2" class="materialize-textarea"></textarea>
                            <label for="icon_prefix2">Mensaje</label>
                        </div>
                        <div class="col s12">
                            <buttom class="waves-effect waves-light orange btn right">Enviar</buttom>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="parallax blue">
        <img src="./assets/img/contact.jpg" class="opacity-2" alt="Unsplashed background img 1">
    </div>
</section>