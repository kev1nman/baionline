<div class="container">
    <div class="section">

        <!--   Icon Section   -->
        <div class="row">
            <div class="col s12 m4">
                <a href="contact.php">
                    <div class="card hoverable">
                        <div class="card-image center-align">
                            <img src="./assets/img/empresas/consorcio.jpg">
                            <a href="contact.php" class="btn-floating btn-large halfway-fab waves-effect white">
                                <i class="material-icons blue-text">work</i>
                            </a>
                        </div>
                        <div class="card-content center-align">
                            <a href="contact.php" class="blue-text">Consorcio</a>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col s12 m4">
                <a href="contact.php">
                    <div class="card hoverable">
                        <div class="card-image center-align">
                            <img src="./assets/img/empresas/comercio.jpg">
                            <a href="contact.php" class="btn-floating btn-large halfway-fab waves-effect white">
                                <i class="material-icons blue-text">work</i>
                            </a>
                        </div>
                        <div class="card-content center-align">
                            <a href="contact.php" class="blue-text">Comercio</a>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col s12 m4">
                <a href="contact.php">
                    <div class="card hoverable">
                        <div class="card-image center-align">
                            <img src="./assets/img/accidentes-personales.jpg">
                            <a href="contact.php" class="btn-floating btn-large halfway-fab waves-effect white">
                                <i class="material-icons blue-text">work</i>
                            </a>
                        </div>
                        <div class="card-content center-align">
                            <a href="contact.php" class="blue-text">Accidentes personales</a>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col s12 m4">
                <a href="contact.php">
                    <div class="card hoverable">
                        <div class="card-image center-align">
                            <img src="./assets/img/cargas.jpg">
                            <a href="contact.php" class="btn-floating btn-large halfway-fab waves-effect white">
                                <i class="material-icons blue-text">work</i>
                            </a>
                        </div>
                        <div class="card-content center-align">
                            <a href="contact.php" class="blue-text">Transporte</a>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col s12 m4">
                <a href="contact.php">
                    <div class="card hoverable">
                        <div class="card-image center-align">
                            <img src="./assets/img/vida.jpg">
                            <a href="contact.php" class="btn-floating btn-large halfway-fab waves-effect white">
                                <i class="material-icons blue-text">work</i>
                            </a>
                        </div>
                        <div class="card-content center-align">
                            <a href="contact.php" class="blue-text">Vida</a>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col s12 m4">
                <a href="contact.php">
                    <div class="card hoverable">
                        <div class="card-image center-align">
                            <img src="./assets/img/empresas/art.jpg">
                            <a href="contact.php" class="btn-floating btn-large halfway-fab waves-effect white">
                                <i class="material-icons blue-text">work</i>
                            </a>
                        </div>
                        <div class="card-content center-align">
                            <a href="contact.php" class="blue-text">A.R.T</a>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col s12 m4">
                <a href="contact.php">
                    <div class="card hoverable">
                        <div class="card-image center-align">
                            <img src="./assets/img/empresas/caucion.jpg">
                            <a href="contact.php" class="btn-floating btn-large halfway-fab waves-effect white">
                                <i class="material-icons blue-text">work</i>
                            </a>
                        </div>
                        <div class="card-content center-align">
                            <a href="contact.php" class="blue-text">Caución</a>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col s12 m4">
                <a href="contact.php">
                    <div class="card hoverable">
                        <div class="card-image center-align">
                            <img src="./assets/img/empresas/flota.jpg">
                            <a href="contact.php" class="btn-floating btn-large halfway-fab waves-effect white">
                                <i class="material-icons blue-text">work</i>
                            </a>
                        </div>
                        <div class="card-content center-align">
                            <a href="contact.php" class="blue-text">Flota vehicular</a>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col s12 m4">
                <a href="contact.php">
                    <div class="card hoverable">
                        <div class="card-image center-align">
                            <img src="./assets/img/personas.jpg">
                            <a href="contact.php" class="btn-floating btn-large halfway-fab waves-effect white">
                                <i class="material-icons blue-text">work</i>
                            </a>
                        </div>
                        <div class="card-content center-align">
                            <a href="contact.php" class="blue-text">Planes afinidad</a>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col s12 m4">
                <a href="contact.php">
                    <div class="card hoverable">
                        <div class="card-image center-align">
                            <img src="./assets/img/empresas/maquinarias.jpg">
                            <a href="contact.php" class="btn-floating btn-large halfway-fab waves-effect white">
                                <i class="material-icons blue-text">work</i>
                            </a>
                        </div>
                        <div class="card-content center-align">
                            <a href="contact.php" class="blue-text">Maquinarias</a>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col s12 m4">
                <a href="contact.php">
                    <div class="card hoverable">
                        <div class="card-image center-align">
                            <img src="./assets/img/empresas/agro.jpg">
                            <a href="contact.php" class="btn-floating btn-large halfway-fab waves-effect white">
                                <i class="material-icons blue-text">work</i>
                            </a>
                        </div>
                        <div class="card-content center-align">
                            <a href="contact.php" class="blue-text">Agro</a>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col s12 m4">
                <a href="contact.php">
                    <div class="card hoverable">
                        <div class="card-image center-align">
                            <img src="./assets/img/empresas/cascos.jpg">
                            <a href="contact.php" class="btn-floating btn-large halfway-fab waves-effect white">
                                <i class="material-icons blue-text">work</i>
                            </a>
                        </div>
                        <div class="card-content center-align">
                            <a href="contact.php" class="blue-text">Cascos (Barcos)</a>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col s12 m4">
                <a href="contact.php">
                    <div class="card hoverable">
                        <div class="card-image center-align">
                            <img src="./assets/img/empresas/responsabilidad_civil.jpg">
                            <a href="contact.php" class="btn-floating btn-large halfway-fab waves-effect white">
                                <i class="material-icons blue-text">work</i>
                            </a>
                        </div>
                        <div class="card-content center-align">
                            <a href="contact.php" class="blue-text">Responsabilidad civil</a>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col s12 m4">
                <a href="contact.php">
                    <div class="card hoverable">
                        <div class="card-image center-align">
                            <img src="./assets/img/empresas/carteles.jpg">
                            <a href="contact.php" class="btn-floating btn-large halfway-fab waves-effect white">
                                <i class="material-icons blue-text">work</i>
                            </a>
                        </div>
                        <div class="card-content center-align">
                            <a href="contact.php" class="blue-text">Carteles y marquesinas</a>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>