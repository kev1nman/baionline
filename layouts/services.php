<div class="container">
    <div class="section">

        <!--   Icon Section   -->
        <div class="row hide-on-med-and-down">
            <div class="col s12 m4">
                <a href="personas.php">
                    <div class="card hoverable">
                        <div class="card-image center-align">
                            <img src="./assets/img/personas.jpg">
                            <a class="btn-floating btn-large halfway-fab waves-effect waves-light white">
                                <i class="material-icons blue-text">people</i>
                            </a>
                        </div>
                        <div class="card-content center-align">
                            <a href="personas.php" class="blue-text">Personas</a>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col s12 m4">
                <a href="auto.php">
                    <div class="card hoverable auto">
                        <div class="card-image center-align">
                            <img src="./assets/img/auto.jpg">
                            <a class="btn-floating btn-large halfway-fab waves-effect waves-light white">
                                <i class="material-icons blue-text">directions_car</i>
                            </a>
                        </div>
                        <div class="card-content center-align">
                            <a href="auto.php" class="blue-text">Auto</a>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col s12 m4">
                <a href="empresas.php">
                    <div class="card hoverable">
                        <div class="card-image center-align">
                            <img src="./assets/img/empresa.jpg">
                            <a href="empresas.php" class="btn-floating btn-large halfway-fab waves-effect waves-light white">
                                <i class="material-icons blue-text">work</i>
                            </a>
                        </div>
                        <div class="card-content center-align">
                            <a href="empresas.php" class="blue-text">Empresa</a>
                        </div>
                    </div>
                </a>
            </div>

        </div>

        <div class="row hide-on-large-only">


            <div class="carousel">
                <div class="carousel-item">
                    <a href="auto.php">
                        <div class="card hoverable">
                            <div class="card-image center-align">
                                <a href="auto.php">
                                    <img src="./assets/img/auto.jpg">
                                </a>
                                <a class="btn-floating btn-large halfway-fab waves-effect waves-light white">
                                    <i class="material-icons blue-text" id="auto-click">directions_car</i>
                                </a>
                            </div>
                            <div class="card-content center-align">
                                <a href="auto.php" class="blue-text" id="auto-click2">Auto</a>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="carousel-item">
                    <a href="personas.php">
                        <div class="card hoverable">
                            <div class="card-image center-align">
                                <img src="./assets/img/personas.jpg">
                                <a class="btn-floating btn-large halfway-fab waves-effect waves-light white">
                                    <i class="material-icons blue-text" id="personas-click">people</i>
                                </a>
                            </div>
                            <div class="card-content center-align">
                                <a href="personas.php" class="blue-text" id="personas-click2">Personas</a>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="carousel-item">
                    <a href="empresas.php">
                        <div class="card hoverable">
                            <div class="card-image center-align">
                                <img src="./assets/img/empresa.jpg">
                                <a class="btn-floating btn-large halfway-fab waves-effect waves-light white">
                                    <i class="material-icons blue-text" id="empresas-click">work</i>
                                </a>
                            </div>
                            <div class="card-content center-align">
                                <a href="empresas.php" class="blue-text" id="empresas-click2">Empresa</a>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>


    </div>
</div>