<?php
  include "./partials/head.php";
  include "./partials/nav.php";
?>
    <section id="header-cotizador" class="parallax-container">
        <div class="section no-pad-bot">
            <div class="container">
                <div class="row">
                    <div class="col s12">
                        <h2 class="header white-text">
                            <span class="line">
                                <img src="./assets/img/line.png" alt="" />A</span>uto
                        </h2>
                        <h5 class="white-text">SEGUROS Y PLANES PARA AUTOMÓVILES</h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="parallax blue">
            <img src="./assets/img/packbaionline.jpg" class="opacity-2" alt="Unsplashed background img 1" />
        </div>
    </section>
    <div class="container">
        <div class="row">
            <div class="col s12 no-pad">
                <div id="contact" class="card no-pad">
                    <div class="card-content">
                        <form action="?" method="GET">
                            <ul class="stepper horizontal" id="horizontal">
                                <li class="step active">
                                    <div data-step-label="" class="step-title waves-effect waves-dark">Año, Marca y Modelo</div>
                                    <div class="step-content" style="left: 0%; display: block;">
                                        <div class="row">
                                            <div class="col s12 m6">
                                                <label>
                                                    Especifique el Año
                                                    <div id="innerCarBr">
                                                        <select id="carYear" class="selecttwo" class="validate" required>
                                                            <option value="2018">2018</option>
                                                            <option value="2018">2017</option>
                                                        </select>
                                                    </div>
                                                </label>
                                            </div>
                                            <div class="col s12 m6">
                                                <label>
                                                    Indique la Marca
                                                    <div id="innerCarBr">
                                                        <select id="carBrand" class="selecttwo">
                                                        </select>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col s12 m6">
                                                <label>
                                                    Indique el Modelo
                                                    <div id="innerCarBr">
                                                        <select id="carModel" class="selecttwo">
                                                        </select>
                                                    </div>
                                                </label>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col s12 m6">
                                                <label>
                                                    asdsad
                                                </label>
                                            </div>

                                        </div>
                                        <div class="step-actions">
                                            <button class="waves-effect waves-dark btn disabled orange next-step">SIGUIENTE</button>
                                        </div>
                                    </div>
                                </li>
                                <li class="step">
                                    <div data-step-label="" class="step-title waves-effect waves-dark">Localidad</div>
                                    <div class="step-content">
                                        <div class="row">
                                            <div class="row">
                                                <div class="input-field col s12 m6">
                                                    <select>
                                                        <option value="" disabled="" selected="">Provincia</option>
                                                        <option value="1">Modelo 1</option>
                                                        <option value="2">Modelo 2</option>
                                                        <option value="3">Modelo 3</option>
                                                    </select>
                                                    <label>Seleccione una Provincia</label>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="input-field col s12 m6">
                                                    <select>
                                                        <option value="" disabled="" selected="">Partido</option>
                                                        <option value="1">Versión 1</option>
                                                        <option value="2">Versión 2</option>
                                                        <option value="3">Versión 3</option>
                                                    </select>
                                                    <label>Seleccione un Partido</label>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="input-field col s12 m6">
                                                    <select>
                                                        <option value="" disabled="" selected="">Localidad</option>
                                                        <option value="1">Localidad 1</option>
                                                        <option value="2">Localidad 2</option>
                                                        <option value="3">Localidad 3</option>
                                                    </select>
                                                    <label>Seleccione una Localidad</label>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="step-actions">
                                            <button class="waves-effect waves-dark btn orange next-step">SIGUIENTE</button>
                                            <button class="waves-effect waves-dark btn-flat previous-step">ATRAS</button>
                                        </div>
                                    </div>
                                </li>
                                <li class="step">
                                    <div data-step-label="" class="step-title waves-effect waves-dark">Cotización</div>
                                    <div class="step-content cotizador">
                                        <div class="row">
                                            <table class="striped centered responsive-table">
                                                <thead>
                                                    <tr class="blue white-text">
                                                        <th class="grey lighten-3 grey-text text-darken-2">Compañía</th>
                                                        <th>
                                                            Opcion 1
                                                            <small>Opción 1</small>
                                                        </th>
                                                        <th>
                                                            Opcion 2
                                                            <small>Opción 2</small>
                                                        </th>
                                                        <th>
                                                            Opcion 3
                                                            <small>Opción 3</small>
                                                        </th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <img src="./assets/img/logo-blanco.png" alt="" />
                                                        </td>
                                                        <td>
                                                            <input class="with-gap" name="group1" type="radio" id="test1" />
                                                            <label for="test1">$ 500.82</label>
                                                        </td>
                                                        <td>
                                                            <input class="with-gap" name="group1" type="radio" id="test2" />
                                                            <label for="test2">$ 1000.82</label>
                                                        </td>
                                                        <td>
                                                            <input class="with-gap" name="group1" type="radio" id="test3" />
                                                            <label for="test3">$ 1500.82</label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img src="./assets/img/logo-blanco.png" alt="" />
                                                        </td>
                                                        <td>
                                                            <input class="with-gap" name="group1" type="radio" id="test4" />
                                                            <label for="test4">$ 500.82</label>
                                                        </td>
                                                        <td>
                                                            <input class="with-gap" name="group1" type="radio" id="test5" checked="" />
                                                            <label for="test5">$ 1000.82</label>
                                                        </td>
                                                        <td>
                                                            <input class="with-gap" name="group1" type="radio" id="test6" />
                                                            <label for="test6" class="green-text">$ 1500.82</label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img src="./assets/img/logo-blanco.png" alt="" />
                                                        </td>
                                                        <td>
                                                            <input class="with-gap" name="group1" type="radio" id="test7" />
                                                            <label for="test7">$ 500.82</label>
                                                        </td>
                                                        <td>
                                                            <input class="with-gap" name="group1" type="radio" id="test8" />
                                                            <label for="test8">$ 1000.82</label>
                                                        </td>
                                                        <td>
                                                            <input class="with-gap" name="group1" type="radio" id="test9" />
                                                            <label for="test9">$ 1500.82</label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img src="./assets/img/logo-blanco.png" alt="" />
                                                        </td>
                                                        <td>
                                                            <input class="with-gap" name="group1" type="radio" id="test10" />
                                                            <label for="test10">$ 500.82</label>
                                                        </td>
                                                        <td>
                                                            <input class="with-gap" name="group1" type="radio" id="test11" />
                                                            <label for="test11">$ 1000.82</label>
                                                        </td>
                                                        <td>
                                                            <input class="with-gap" name="group1" type="radio" id="test12" />
                                                            <label for="test12">$ 1500.82</label>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="step-actions position-relative">
                                            <button class="waves-effect waves-dark btn orange next-step">SIGUIENTE</button>
                                            <button class="waves-effect waves-dark btn-flat previous-step">ATRAS</button>
                                        </div>
                                    </div>
                                </li>
                                <li class="step" data-last="true">
                                    <div class="step-title waves-effect waves-dark">Datos para emisión</div>
                                    <div class="step-content">
                                        <div class="row">
                                            <div class="input-field col s12 m6 grey-text">
                                                <i class="material-icons prefix">account_circle</i>
                                                <input id="icon_prefix" type="text" class="validate" />
                                                <label for="icon_prefix">Nombre</label>
                                            </div>
                                            <div class="input-field col s12 m6 grey-text">
                                                <i class="material-icons prefix">phone</i>
                                                <input id="icon_telephone" type="tel" class="validate" />
                                                <label for="icon_telephone">Teléfono</label>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="input-field col s12 m6 grey-text">
                                                <i class="material-icons prefix">account_circle</i>
                                                <input id="icon_prefix" type="text" class="validate" />
                                                <label for="icon_prefix">Apellido</label>
                                            </div>
                                            <!-- Switch -->
                                            <div class="switch col s12 m6">
                                                <label>
                                                    <div class="chip chip-large">
                                                        <span class="fa fa-whatsapp"></span> Whatsapp
                                                    </div>
                                                    <br /> No
                                                    <input type="checkbox" />
                                                    <span class="lever"></span>
                                                    Si
                                                </label>
                                            </div>
                                        </div>
                                        <div class="step-actions">
                                            <button class="waves-effect waves-dark btn btn-large orange" type="submit">ENVIAR</button>
                                        </div>
                                    </div>
                                </li>
                                <li class="step" data-last="true">
                                    <div class="step-title waves-effect waves-dark">Listo!</div>
                                    <div class="step-content green-text">

                                        <div class="row">
                                            <div class="col s12 center-align">
                                                <h5>
                                                    <i class="large material-icons">check</i>
                                                    <p class="flow-text center-align">
                                                        Lo has Logrado!, en el transcurso de 15 minutos un Productor Asesor de Seguros se pondrá en contacto con vos para contarte
                                                        los beneficios.
                                                    </p>
                                                </h5>
                                            </div>
                                        </div>
                                        <div class="step-actions">
                                            <button class="waves-effect waves-dark btn btn-large orange" type="submit">FINALIZAR</button>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
  include "./layouts/contact.php";
  include "./partials/footer.php";
?>

        