'use strict';

var fillDropdown = function (url, target) {
  $(target).material_select('destroy');

  $.getJSON(url, function (data) {
    $(target).empty();

    $(target).removeAttr('disabled');

    $(target).append(
      $('<option></option>')
      .html('Marca')
      .addClass('disabled')
    );

    $.each(data, function (index, item) {
      $(target).append(
        $('<option></option>')
        .val(item.id)
        .html(item.nombre)
      );
    });

    $(target).material_select();
  });
};

var fillDropdownSelect2 = function (url, target, nullText) {
  var _target = $(target);

  $.getJSON(url, function (data) {
    _target
      .empty()
      .removeAttr('disabled')
      .append(
        $('<option></option>')
        .html(nullText)
        .addClass('disabled')
      );

    $.each(data, function (index, item) {
      _target.append(
        $('<option></option>')
        .val(item.id)
        .html(item.name)
      );
    });

    _target.select2();
  });
};

var fillCarModel = function (url, target) {
  $(target).material_select('destroy');

  $.getJSON(url, function (data) {
    $(target).empty();

    $(target).removeAttr('disabled');

    $(target).append(
      $('<option></option>')
      .html('Modelo')
      .addClass('disabled')
    );

    $.each(data.modelos, function (index, item) {
      $(target).append(
        $('<option></option>')
        .val(item.id)
        .html(item.name)
      );
    });

    $(target).select2();
  });
}




$('.provincia-select').select2();
$('.partido-select').select2();
$('.localidad-select').select2();
