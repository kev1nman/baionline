var baseUrl = 'https://autocenter1.azurewebsites.net/api';

var yearMethod = '/Anios';
var brandMethod = '/Anios';

var carBrandSelector = '#carBrand';
var carModelSelector = '#carModel';
var carYearSelector = '#carYear';

var onYearChange = function (e) {

  $(carBrandSelector).off('change');

  e.preventDefault();

  var jthis = $(this);

  var yearId = parseInt(jthis.val());

  var modeloUrl = baseUrl + brandMethod + '/' + yearId + '/Marcas';

  fillDropdownSelect2(modeloUrl, carBrandSelector, 'Marca');

  $(carBrandSelector).on('change', onBrandChange);

};

var onModelChange = function (e) {

  e.preventDefault();

  var jthis = $(this);

  
  var modelId = parseInt($(carModelSelector).val());


  if (!isNaN(modelId)) {
    alert(modelId);
  }
};

var onBrandChange = function (e) {

  $(carModelSelector).on('change', onModelChange);

  e.preventDefault();

  var jthis = $(this);

  var yearId = parseInt($(carYearSelector).val());

  var brandId = parseInt(jthis.val());

  var modeloUrl = baseUrl + brandMethod + '/' + yearId + '/Marca/' + brandId;

  fillDropdownSelect2(modeloUrl, carModelSelector, 'Modelo');

  $(carModelSelector).on('change', onModelChange);

};

$(function () {

  fillDropdownSelect2(baseUrl + yearMethod, carYearSelector, 'Año del Vehiculo');

  $(carYearSelector).on('change', onYearChange).select2();

});