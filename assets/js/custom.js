

$(function () {

    var menu = $('.menu');
    var target = $('#' + menu.attr("data-target"));
    var cambio = false;


    $('.button-collapse').sideNav();
    $('.parallax').parallax();

    $('select:not(.selecttwo)').material_select(); 

    //$('select').material_select();
    $('.carousel').carousel();
    $('.modal').modal();
    $('.tabs').tabs({ swipeable: false });

    menu.pushpin({
        top: target.offset().top,
    });

    $('nav ul li a').each(function (index) {
        if (this.href.trim() == window.location) {
            $(this).parent().addClass("active");
            cambio = true;
        }
    });

    if (!cambio) {
        $('nav ul li:nth-child(0)').addClass("active");
    }

    $('#auto-click').on('click touchstart', e => {
        e.preventDefault();
        window.location = "auto.php";
    });

    $('#auto-click2').on('click touchstart', e => {
        e.preventDefault();
        window.location = "auto.php";
    });

    $('#personas-click').on('click touchstart', e => {
        e.preventDefault();
        window.location = "personas.php";
    });

    $('#personas-click2').on('click touchstart', e => {
        e.preventDefault();
        window.location = "personas.php";
    });

    $('#empresas-click').on('click touchstart', e => {
        e.preventDefault();
        window.location = "empresas.php";
    });

    $('#empresas-click2').on('click touchstart', e => {
        e.preventDefault();
        window.location = "empresas.php";
    });

});